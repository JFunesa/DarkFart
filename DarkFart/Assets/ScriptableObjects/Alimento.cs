using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Alimento", menuName = "Item/Consumible/Alimento")]
public class Alimento : Consumible, IConsume
{
    [SerializeField] private GameEventFloat m_EatFood;
    [SerializeField] private float m_Calories;
    public float Calories
    {
        get
        {
            return m_Calories;
        }
        set
        {
            m_Calories = value;
        }
    }

    public void Consume()
    {
        m_EatFood.Raise(m_Calories);
    }
}
