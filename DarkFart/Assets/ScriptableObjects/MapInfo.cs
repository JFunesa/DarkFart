using System;
using System.Collections.Generic;
using UnityEngine;
using static Backpack;

[CreateAssetMenu]
[Serializable]
public class MapInfo : ScriptableObject
{
    public List<ItemInfo> items = new List<ItemInfo>();
    public List<DropeableInfo> resources = new List<DropeableInfo>();
    public List<AnimalInfo> animals = new List<AnimalInfo>();
    public List<EnemyInfo> enemies = new List<EnemyInfo>();
    [SerializeField]private bool alreadyGenerated = false;
    public bool AlreadyGenerated { get { return alreadyGenerated; } set { alreadyGenerated = value; } }

    public void Save()
    {
        SaveGameObjects();

        SaveData.Instance.Save();
    }
    private void SaveGameObjects()
    {
        IdInfo[] infos = FindObjectsByType<IdInfo>(FindObjectsSortMode.None);

        ClearInfo();
        foreach (IdInfo info in infos)
        {
            info.SaveInfo();
        }
    }

    public void ClearInfo()
    {
        items.Clear();
        resources.Clear();
        enemies.Clear();
        animals.Clear();
    }
    public void ClearAllInfo()
    {
        ClearInfo();
        alreadyGenerated = false;
    }
}

[Serializable]
public class GameObjectsInfo
{
    public int id;
    public int layer;
    public Vector3 pos;
    public Sprite sprite;
    public enum typeOfGameObject { ArbolRoca, Enemy, Animal, Item, Estructura, Planta, NPC}
    public typeOfGameObject type;

    public GameObjectsInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id)
    {
        this.layer = layer;
        this.pos = pos;
        this.sprite = sprite;
        this.type = type;
        this.id = id;
    }
}

[Serializable]
public class NPCInfo : GameObjectsInfo
{
    public float nivelAmistad;
    public PersonalidadInfo personalidad;
    [Serializable]
    public enum typeOfScript { None, RepararBarrera, DefenderGranja }
    public typeOfScript script;

    //stats
    public string name;
    public float hp;
    public float MaxHP;
    public float speed;

    public NPCInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}

[Serializable]
public class EstructuraInfo : GameObjectsInfo
{
    public Estructura estructura;
    public EnumForEstructures typeEstructure;

    public EstructuraInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}

[Serializable]
public class BarreraInfo : EstructuraInfo
{
    public float vida;
    public float maxVida;
    public BarreraInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}

[Serializable]
public class CofreInfo : EstructuraInfo
{
    public ItemSlot[] items;
    public CofreInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}

[Serializable]
public class CalderoInfo : EstructuraInfo
{
    public int water;
    public int maxWater;
    public ItemSlot wood;
    public CalderoInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}

[Serializable]
public class ItemInfo : GameObjectsInfo
{
    public Item item;
    public int amount;

    public ItemInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}
[Serializable]
public class PlantInfo : GameObjectsInfo
{
    public bool isReady;
    public bool isDead;
    public int currentDays;
    public int days;
    public Sprite[] sprites = new Sprite[3];
    public Item item;
    public PlantInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}

[Serializable]
public class DropeableInfo: GameObjectsInfo
{
    public List<Item> items = new List<Item>();
    public float vida;
    public float maxVida;

    public DropeableInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}
[Serializable]
public class AnimalInfo : DropeableInfo
{
    public Animal animal;

    public AnimalInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}
[Serializable]
public class EnemyInfo : DropeableInfo
{
    public Enemy enemy;

    public EnemyInfo(int layer, Vector3 pos, Sprite sprite, typeOfGameObject type, int id) : base(layer, pos, sprite, type, id)
    {
    }
}
