using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armadura", menuName = "Item/Equipable/Armadura")]
public class Armadura : Equipable
{
    [SerializeField]
    private float hpExtra;
    public float HpExtra
    {
        get
        {
            return hpExtra;
        }
        set
        {
            hpExtra = value;
        }
    }
}
