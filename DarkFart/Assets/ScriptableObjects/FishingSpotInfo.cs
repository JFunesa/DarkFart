using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;
using System;

[CreateAssetMenu]
public class FishingSpotInfo : ScriptableObject
{
    [Serializable]
    public struct FishingSpot
    {
        public List<Pez> fishPool;
        public Tile tile;
    }

    public List<FishingSpot> fishingTilesInfo;



}
