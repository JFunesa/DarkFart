using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Craft", menuName = "Craft/Craft")]
public class Craft : ScriptableObject
{
    [Serializable]
    public class Ingredients
    {
        [SerializeField]
        public Item ingredient;
        [SerializeField]
        public int quantity;
    }

    [SerializeField]
    private string description;
    public string Description
    {
        get
        {
            return description;
        }
        set
        {
            description = value;
        }
    }
    [SerializeField]
    private List<Ingredients> ingredients;
    public List<Ingredients> IngredientsCraft
    {
        get
        {
            return ingredients;
        }
        set
        {
            ingredients = value;
        }
    }

    [SerializeField]
    private Item result;
    public Item Result
    {
        get
        {
            return result;
        }
        set
        {
            result = value;
        }
    }
}
