using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Equipable", menuName = "Item/Equipable")]
public class Equipable : Item
{
    [SerializeField]
    private float energiaQueGasta;
    public float EnergiaQueGasta
    {
        get
        {
            return energiaQueGasta;
        }
        set
        {
            energiaQueGasta = value;
        }
    }
}
