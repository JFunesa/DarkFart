using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using static UnityEngine.RuleTile.TilingRuleOutput;

[CreateAssetMenu(fileName = "Cuchillo", menuName = "Item/Equipable/Herramientas/Cuchillo")]
public class Cuchillo : Equipable, IActionable
{
    [SerializeField] private GameEventFloat m_WasteEnergy;
    [SerializeField] private LayerMask m_AnimalMask;
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        Vector3 target = position + direction;

        Vector3 origin = target;
        origin.z = -10;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_AnimalMask);

        if (hit.transform == null) return;

        VacaZombieController controller = null;
        if (hit.transform.gameObject.TryGetComponent<VacaZombieController>(out controller))
        {
            if (controller.IsAble)
            {
                Alimento c = controller.GetCarne();
                InventarioController.Instance.AddItem(c);
            }
            
        }
    }
}
