using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - ItemAndPos")]
public class GameEventItemAndPos : GameEvent<Item,int,int,int> { }

