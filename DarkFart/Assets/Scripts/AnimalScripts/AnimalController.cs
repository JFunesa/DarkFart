using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using static UnityEditor.PlayerSettings;

public class AnimalController : MonoBehaviour
{
    private Rigidbody2D m_Rigidbody;
    private Vector2 m_Movement;
    private float m_SQDistancePerFrame;
    private NavMeshAgent m_NavMeshAgent;
    private float displacementDist = 5f;

    [Header("ScriptableObject_Animal")]
    [SerializeField]
    private Animal m_AnimalSO;
    public Animal animal { get { return m_AnimalSO; } set { m_AnimalSO = value; } }

    [Header("Animal Values")]
    private string m_Name;
    private float m_Speed = 2;
    [SerializeField]
    private float m_Drag;
    [SerializeField]
    private float m_Acceleration;

    private Transform m_PlayerTransform;
    private Transform m_EnemyTransform;
    private Coroutine m_CoroutineRope;
    public bool cuerda { get { return cuerda; } set { cuerda = value; } }

    public enum SwitchMachineStates { NONE, IDLE, RANDOMWALK, CHASE, RUNAWAY };
    public SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
        Debug.Log("State: " + newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //Debug.Log("AnimalState: IDLE");

                break;

            case SwitchMachineStates.RANDOMWALK:
                //Debug.Log("AnimalState: RANDOMWALK");
                StartCoroutine(WalkRandomly());
                break;

            case SwitchMachineStates.CHASE:
                //Debug.Log("AnimalState: CHASE");
                StartCoroutine(Recalculate(m_PlayerTransform));
                break;

            case SwitchMachineStates.RUNAWAY:
                //Debug.Log("AnimalState: RUNAWAY");
                //StartCoroutine(RunAway(m_EnemyTransform));
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.RANDOMWALK:

                break;

            case SwitchMachineStates.CHASE:
                StopAllCoroutines();
                break;

            case SwitchMachineStates.RUNAWAY:
                //StopCoroutine(RunAway(m_EnemyTransform));

                break;



            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;
            case SwitchMachineStates.RANDOMWALK:

                break;

            case SwitchMachineStates.CHASE:



                break;
            case SwitchMachineStates.RUNAWAY:

                Vector3 normDir = (m_EnemyTransform.position - transform.position).normalized;
                m_NavMeshAgent.SetDestination(transform.position - (normDir * displacementDist));

                break;


            default:
                break;
        }
    }


    void Awake()
    {

        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_SQDistancePerFrame = 3 * m_Speed * Time.fixedDeltaTime;
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_NavMeshAgent.updateUpAxis = false;
        m_NavMeshAgent.updateRotation = false;

        m_Speed = m_AnimalSO.Speed;
        m_Name = m_AnimalSO.Name;
        m_NavMeshAgent.speed = m_Speed;

        GetComponent<EntitieDie>().Drops = m_AnimalSO.Drops;
        GetComponent<VidaController>().Hp = m_AnimalSO.HP;
        GetComponent<VidaController>().MaxHP = m_AnimalSO.HP;

        m_PlayerTransform = FindAnyObjectByType(typeof(MovimentPlayer)).GetComponent<Transform>();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.RANDOMWALK);
    }

    void Update()
    {
        UpdateState();
    }



    private void FixedUpdate()
    {
        if (m_CurrentState != SwitchMachineStates.RANDOMWALK) return;
        Vector2 groundSpeed = Vector2.right * m_Rigidbody.velocity.x + Vector2.up * m_Rigidbody.velocity.y;

        if (groundSpeed.magnitude < m_Speed)
        {
            m_Rigidbody.AddForce((m_Movement * m_Speed) * m_Acceleration, ForceMode2D.Force);
        }


        groundSpeed = Vector2.right * m_Rigidbody.velocity.x + Vector2.up * m_Rigidbody.velocity.y;
        //Drag
        m_Rigidbody.AddForce(-groundSpeed * m_Drag, ForceMode2D.Force);
    }

    private IEnumerator WalkRandomly()
    {
        while (true)
        {

            int rx = Random.Range(-1, 2);
            int ry = Random.Range(-1, 2);

            m_Movement = new Vector2(rx, ry);
            yield return new WaitForSeconds(2);
        }
    }

    private IEnumerator Recalculate(Transform destination)
    {
        while (true)
        {
            //Debug.Log("recalculando");
            m_NavMeshAgent.SetDestination(destination.position);

            yield return new WaitForSeconds(0.5f);
        }

    }

    public void ChangeToChasing()
    {
        ChangeState(SwitchMachineStates.CHASE);
    }
    public void ChangeToRunAway(Transform transform)
    {
        m_EnemyTransform = transform;
        ChangeState(SwitchMachineStates.RUNAWAY);
    }
    public void ChangeToIdle()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    public void ChangeToRandomWalk()
    {
        m_EnemyTransform = null;
        ChangeState(SwitchMachineStates.RANDOMWALK);
    }

    public void startDraw(LineRenderer lr)
    {
        m_CoroutineRope = StartCoroutine(DrawRope(lr));
    }

    public void stopDraw()
    {
        StopCoroutine(m_CoroutineRope);
        Destroy(GetComponent<LineRenderer>());
    }

    private IEnumerator DrawRope(LineRenderer lr)
    {
        while (true)
        {
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, m_PlayerTransform.position);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public void Inicia(Vector3 pos)
    {
        transform.position = new Vector3(pos.x, pos.y, 0);
        gameObject.SetActive(true);
    }
}
