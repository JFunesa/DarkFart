using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(GameEventListener))]
public class PlantsGrowths : MonoBehaviour, Interactuable
{   
    [SerializeField] private bool isReady = false;
    public bool IsReady { get { return isReady; } set { isReady = value; } }
    [SerializeField] private bool isDead = false;
    public bool IsDead { get { return isDead; } set {
            if (value) 
                Die(); 
        } }
    private SpriteRenderer m_SpriteRenderer;
    private int inter;
    [SerializeField] private int currentDays = 0;
    public int CurrentDays { get { return currentDays; } set { currentDays = value; } }
    [SerializeField]
    private int days;
    public int Days { get { return days; } set {  days = value; } }
    [SerializeField]
    private Sprite[] m_Sprites;
    public Sprite[] Sprites { get { return m_Sprites; } set { m_Sprites = value; } }
    [SerializeField]
    private Item m_item;
    public Item Item { get { return m_item; } set { m_item = value; } }
    private Tilemap m_Tilemap;
    [SerializeField]
    private Tile m_WateredTile;
    [SerializeField]
    private Tile m_DryTile;
    private void Awake()
    {
        inter = days / 2;
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteRenderer.sprite = m_Sprites[0];
        m_Tilemap = TilemapController.PlantasTilemap;
    }

    public void NewDay()
    {
        if (isReady || isDead) return;
        if(isWatered())
        {
            m_Tilemap.SetTile(m_Tilemap.WorldToCell(transform.position), m_DryTile);
            currentDays++;
            if (currentDays == days)
            {
                isReady = true;
                m_SpriteRenderer.sprite = m_Sprites[2];
            }
            else if (currentDays >= inter)
                m_SpriteRenderer.sprite = m_Sprites[1];
        }
        else
        {
            Die();
        }
    }

    private bool isWatered()
    {
        return m_WateredTile == m_Tilemap.GetTile<Tile>(m_Tilemap.WorldToCell(transform.position));
    }

    public void Interact()
    {
        if(!isReady) return;

        ItemInWorld item = gameObject.AddComponent<ItemInWorld>();
        item.SetItem(m_item);
    }

    private void Die()
    {
        isDead = true;
        m_SpriteRenderer.sprite = m_Sprites[3];
    }
}
