using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(MovimentPlayer))]
public class TakeItems : MonoBehaviour
{
    private InventarioController m_InventarioController;
    [Header("TEST ITEMS")]
    [SerializeField]
    private Equipable testArma;
    [SerializeField]
    private Materiales testMaterial;
    [SerializeField] private LayerMask estructuraLayer;
    private MovimentPlayer m_MovimentPlayer;



    void Start()
    {
        m_InventarioController = InventarioController.Instance;
        m_MovimentPlayer = GetComponent<MovimentPlayer>();
        //m_InventarioController.getBackpack().ClearBackpack();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (m_InventarioController.getBackpack().CountOccupiedSlots() < 36)
            {
                m_InventarioController.AddItem(testArma);
            }
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (m_InventarioController.getBackpack().CountOccupiedSlots() < 36)
            {
                m_InventarioController.AddItem(testMaterial, 99);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject objecte = collision.gameObject;
        if (objecte != null)
        {
            //Debug.Log(collision.gameObject.name);
            if (objecte.TryGetComponent<ItemInWorld>(out ItemInWorld item))
            {
                if (m_InventarioController.getBackpack().CountOccupiedSlots(item.returnItem(), item.returnAmount()))
                {
                    int saveAmount = m_InventarioController.AddItem(item.returnItem(), item.returnAmount());
                    if (saveAmount > 0)
                        item.SetItem(item.returnItem(), saveAmount);
                    else
                        Destroy(objecte.gameObject);
                    //objecte.SetActive(false);

                }
                else
                {
                    Debug.Log("No se puede a�adir");
                }
            }
        }
    }

    public void RecolocarEstructura(InputAction.CallbackContext context)
    {
        Debug.Log("Position: " + transform.position);
        Vector3 origin = transform.position;
        origin.z = -10;
        origin.x += m_MovimentPlayer.LastInput.x;
        origin.y += m_MovimentPlayer.LastInput.y;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, estructuraLayer);


        Debug.Log("Hit1: " + transform.position);

        if (hit.transform == null) return;

        Debug.Log("Hit2: " + hit.transform.position);
        Debug.Log("Hit3: " + hit.transform.gameObject.name);

        if (!hit.transform.gameObject.TryGetComponent<EstructuraBehaviour>(out EstructuraBehaviour e)) return;

        Debug.Log("Estructura: " + e.Estructura.name);
        m_InventarioController.AddItem(e.Estructura, 1);
        Destroy(hit.transform.gameObject);
    }
}
