using UnityEngine;

public class ItemInWorld : MonoBehaviour
{
    [SerializeField] private bool inicial = false;
    [SerializeField] private Item item;
    [SerializeField] private int m_Amount;
    [SerializeField] private Sprite m_SpriteDefault;
    private SpriteRenderer m_SpriteRenderer;
    private void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        if (inicial) Inicia(item, m_Amount, transform.position);
    }

    public Item returnItem()
    {
        return item;
    }

    public int returnAmount()
    {
        return m_Amount;
    }

    public void SetItem(Item i, int quantity)
    {
        item = i;
        m_Amount = quantity;
    }

    public void SetItem(Item i)
    {
        item = i;
        m_Amount = 1;
    }

    public void Inicia(Item i, int quantity, Vector3 pos)
    {
        SetItem(i, quantity);
        if (item.Sprite != null)
            m_SpriteRenderer.sprite = item.Sprite;
        else
            m_SpriteRenderer.sprite = m_SpriteDefault;
        transform.position = new Vector3(pos.x, pos.y, 0);
        gameObject.SetActive(true);
    }

    public void Inicia(Item i, Vector3 pos)
    {
        SetItem(i);
        if (item.Sprite != null)
            m_SpriteRenderer.sprite = item.Sprite;
        else
            m_SpriteRenderer.sprite = m_SpriteDefault;
        transform.position = new Vector3(pos.x, pos.y, 0);
        gameObject.SetActive(true);
    }
}
