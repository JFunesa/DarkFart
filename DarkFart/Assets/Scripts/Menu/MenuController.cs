using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

public class MenuController : MonoBehaviour
{
    private InputController _controller;
    private guiController canvas;
    private MovimentPlayer player;
    private GameController gameController;
    [SerializeField] private GranjaInfo granjaInfo;
    [SerializeField] private MapInfo mapInfo;
    [SerializeField] private Backpack backpack;

    private void Start()
    {
        canvas = FindFirstObjectByType<TagCanvas>().gameObject.GetComponent<guiController>();
        gameController = FindFirstObjectByType<GameController>();
        player = FindFirstObjectByType<MovimentPlayer>();

        canvas.gameObject.SetActive(false);
        gameController.gameObject.SetActive(false);
        player.enabled = false;
    }

    public void NewGame()
    {
        granjaInfo.ClearAllInfo();
        mapInfo.ClearAllInfo();
        backpack.ClearAllInfo();
        SceneController.Instance.ChangeScene(SceneController.SCENES.Granja);
        startAll();
    }

    public void Load()
    {
        string scene = SaveData.Instance.Load();
        SceneController.Instance.ChangeScene(SceneController.Instance.FromStringToScene(scene));
        startAll();
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game is exiting");
    }

    private void startAll()
    {
        SceneController.Instance.startAll();
        gameController.gameObject.SetActive(true);
    }
}
