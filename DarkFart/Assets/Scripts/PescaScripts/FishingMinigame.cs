using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FishingMinigame : MonoBehaviour
{
    [Header("FishInfo")]
    [SerializeField]
    private Pez fishInfo;
    [SerializeField]
    private Transform m_PlayerTransform;
    public Pez FishInfo
    {
        get { return fishInfo; }
        set { fishInfo = value; }
    }

    [Header("Parameters")]
    [SerializeField]
    private LayerMask m_LayerMask;
    [SerializeField]
    private float catchPoints;
    [SerializeField] 
    private float decreaseCatchAmount;
    [SerializeField]
    private float fillAmount;
    [SerializeField]
    private float unfillAmount;
    [SerializeField]
    private Canvas canvas;

    

    [Header("GameEvents")]
    [SerializeField]
    private GameEventFloat FillAmount;
    [SerializeField]
    private GameEvent FishingWin;


    public void Ini()
    {
        catchPoints = fishInfo.StartCatchAmount;
        decreaseCatchAmount = fishInfo.DecreaseCatchAmount;
        fillAmount = fishInfo.FillAmount;
        unfillAmount = fishInfo.UnfillAmount;
        canvas.worldCamera = FindFirstObjectByType<Camera>();
        GetComponentInChildren<SquareMovement>().Speed = fishInfo.Speed;
        GetComponentInChildren<SquareMovement>().Size = fishInfo.Size;
        GetComponentInChildren<SquareMovement>().Ini(m_PlayerTransform.position);
        StartCoroutine(DecreaseCatch());
    }

    IEnumerator DecreaseCatch()
    {
        while (true)
        {
            catchPoints -= decreaseCatchAmount;
            FillAmount.Raise(catchPoints);
            if (catchPoints <= 0)
            {
                StopFishing();
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void FishingMiniGame(InputAction.CallbackContext context)
    {
        Vector3 raycastorigin = m_PlayerTransform.position;
        raycastorigin.z = -10;
        RaycastHit2D hit = Physics2D.Raycast(raycastorigin, Vector3.forward, 30, m_LayerMask);
        if (hit.rigidbody != null)
        {
            catchPoints += fillAmount;
            if (catchPoints >= 1)
            {
                InventarioController.Instance.getBackpack().Add(fishInfo.PezAlimento);
                InventarioController.Instance.getDisplayBackpack().RefreshBackpack();
                StopFishing();
            }

        }
        else
        {
            catchPoints -= unfillAmount;
            if (catchPoints <= 0)
            {
                StopFishing();
            }
        }
        FillAmount.Raise(catchPoints);
    }

    private void StopFishing()
    {
        StopAllCoroutines();
        GameController.Instance.StopFishing();
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }
}
