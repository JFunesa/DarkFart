using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareMovement : MonoBehaviour
{
    private Rigidbody2D m_Rigidody;
    private Transform m_Transform;

    [SerializeField]
    private float m_Speed;
    private Vector3 m_Position;
    public float Speed
    {
        get
        {
            return m_Speed;
        }
        set
        {
            m_Speed = value;
        }
    }
    [SerializeField]
    private float m_Size;
    public float Size
    {
        get
        {
            return m_Size;
        }
        set
        {
            m_Size = value;
        }
    }
    private void Awake()
    {
        m_Rigidody = GetComponent<Rigidbody2D>();
        m_Transform = GetComponent<Transform>();
        
    }
    // Start is called before the first frame update
    public void Ini(Vector3 pos)
    {
        m_Rigidody.velocity = Vector3.right * m_Speed;
        GetComponent<BoxCollider2D>().size = new Vector2(m_Size, m_Size);
        m_Position = pos;
        transform.position = m_Position;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Transform.position.x <= m_Position.x-5)
        {
            m_Rigidody.velocity = Vector3.right * m_Speed;
        } 
        if (m_Transform.position.x >= m_Position.x + 5) 
        { 
            m_Rigidody.velocity = Vector3.left * m_Speed;
        }
        
    }
}
