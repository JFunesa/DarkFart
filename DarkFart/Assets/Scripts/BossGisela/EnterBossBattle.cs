using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterBossBattle : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Door;
    [SerializeField]
    private GameObject m_Boss;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_Door.SetActive(true);
        m_Boss.SetActive(true);
        gameObject.SetActive(false);
    }
}
