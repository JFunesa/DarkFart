using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHitboxDamage : MonoBehaviour
{
    [SerializeField]
    private float damage;
    public float Damage { set { damage = value; } }
    [SerializeField]
    private int critProb;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        int r = Random.Range(1, 101);
        float realDamage = damage;
        if (r <= critProb)
        {
            realDamage *= 2;
        }
        if (collision.TryGetComponent<VidaController>(out VidaController vida))
            vida.LessHP(realDamage);

    }
}
