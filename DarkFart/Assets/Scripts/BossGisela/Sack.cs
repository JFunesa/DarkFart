using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.AI;

public class Sack : MonoBehaviour
{
    private Animator m_Animator;
    private NavMeshAgent m_NavMeshAgent;

    [SerializeField]
    private Transform m_PlayerTransform;
    [SerializeField]
    private Transform m_PivotHitbox;
    [SerializeField]
    private ObjectPool m_CorpsePool;
    [SerializeField]
    private BossHitboxDamage m_BossHitboxDamage;

    [SerializeField]
    private AttacksCooldown[] fase1Attacks;
    [SerializeField]
    private AttacksCooldown[] fase2Attacks;
    [SerializeField]
    private AttacksCooldown[] fase3Attacks;

    private AttacksCooldown[] currentFaseAttacks;

    [SerializeField]
    private AttacksCooldown currentAttack;

    [SerializeField]
    private bool playerInRange=false;

    [Header("Fase1 Stats")]
    [SerializeField]
    private float fase1Damage;
    [SerializeField]
    private float fase1Speed;
    [Header("Fase2 Stats")]
    [SerializeField]
    private float fase2Damage;
    [SerializeField]
    private float fase2Speed;
    [Header("Fase3 Stats")]
    [SerializeField]
    private float fase3Damage;
    [SerializeField]
    private float fase3Speed;

    [Header("Corpse Explosion Attack")]
    [SerializeField]
    private int amountCorpses;
    [SerializeField]
    private float speedCorpses;

    private List<GameObject> m_CorpsesUsed = new List<GameObject>();

    


    public bool PlayerInRange { set { playerInRange = value; } }
    private enum SwitchMachineStates { NONE, IDLE, CHASING, STUNNED, ATTACKING };
    private SwitchMachineStates CurrentState;
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == CurrentState)
            return;

        ExitState();
        InitState(newState);
    }
    private void InitState(SwitchMachineStates currentState)
    {
        CurrentState = currentState;
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:
                Debug.Log("Sack: IDLE");

                break;
            case SwitchMachineStates.CHASING:
                m_NavMeshAgent.isStopped = false;
                Debug.Log("Sack: CHASING");
                StartCoroutine(Recalculate());

                break;

            case SwitchMachineStates.STUNNED:
                Debug.Log("Sack: STUNNED");
                m_Animator.Play("sack_stunned");
                break;

            case SwitchMachineStates.ATTACKING:
                Debug.Log("Sack: ATTACKING");
                m_NavMeshAgent.isStopped = true;
                Vector3 vRotation = transform.position - m_PlayerTransform.position;
                vRotation.z = 0;
                m_PivotHitbox.up = vRotation;

                m_Animator.Play(currentAttack.attack.ToString());


                break;
            default:
                break;
        }
    }
    private void ExitState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.CHASING:
                Vector3 vRotation = transform.position - m_PlayerTransform.position;
                vRotation.z = 0;
                m_PivotHitbox.up = vRotation;
                if (m_NavMeshAgent.enabled)
                    m_NavMeshAgent.SetDestination(transform.position);
                StopAllCoroutines();

                break;

            case SwitchMachineStates.STUNNED:
                if (m_NavMeshAgent.enabled)
                    m_NavMeshAgent.SetDestination(transform.position);

                break;

            case SwitchMachineStates.ATTACKING:
                

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;
            case SwitchMachineStates.CHASING:
                Vector3 vRotation = transform.position - m_PlayerTransform.position;
                vRotation.z = 0;
                m_PivotHitbox.up = vRotation;

                break;
            case SwitchMachineStates.STUNNED:

                break;
            case SwitchMachineStates.ATTACKING:

                /*Vector3 vRotation2 = transform.position - m_PlayerTransform.position;
                vRotation2.z = 0;
                m_PivotHitbox.up = vRotation2;*/
                break;
            default:
                break;
        }
    }

    private void Awake()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_NavMeshAgent.updateUpAxis = false;
        m_NavMeshAgent.updateRotation = false;
        m_Animator = GetComponent<Animator>();
        m_NavMeshAgent.speed = fase1Speed;
        m_BossHitboxDamage.Damage = fase1Damage;
        m_PlayerTransform = FindAnyObjectByType<MovimentPlayer>().transform;
    }
    private void Start()
    {
        currentFaseAttacks = fase1Attacks;
        InitState(SwitchMachineStates.CHASING);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            currentAttack = new AttacksCooldown(AttacksCooldown.Attack.Attack3, 7, false);
            m_Animator.Play(AttacksCooldown.Attack.Attack3.ToString());
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            m_Animator.Play("Attack4");
        }

        UpdateState();


    }

    public IEnumerator FinalAttack()
    {
        
        ChangeState(SwitchMachineStates.STUNNED);
        
        yield return new WaitForSeconds(currentAttack.cooldown);
        int r = UnityEngine.Random.Range(0, currentFaseAttacks.Length);
        currentAttack = currentFaseAttacks[r];
        
        if (currentAttack.needsRange)
        {
            
            if (playerInRange) ChangeState(SwitchMachineStates.ATTACKING);
            else ChangeState(SwitchMachineStates.CHASING);

        } else
        {
            
            ChangeState(SwitchMachineStates.ATTACKING);
        }
        
    }

    private IEnumerator Recalculate()
    {
        while (true)
        {
            
            if (m_NavMeshAgent.enabled && m_PlayerTransform != null)
                m_NavMeshAgent.SetDestination(m_PlayerTransform.position);

            if (playerInRange)
            {
                ChangeState(SwitchMachineStates.ATTACKING);
                break;
            }

            yield return new WaitForSeconds(0.5f);
        }

    }

    public void TpToPlayer()
    {
        transform.position = m_PlayerTransform.position;
    }

    public void LanzarCuerpos()
    {
        for (int i = 0; i < amountCorpses; ++i)
        {
            GameObject c = m_CorpsePool.GetPooledObject();
            c.SetActive(true);
            float randomX = UnityEngine.Random.Range(-1f , 1f);
            float randomY = UnityEngine.Random.Range(-1f, 1f);
            Debug.Log(new Vector2(randomX, randomY).normalized * speedCorpses);
            c.transform.position = transform.position;
            c.GetComponent<Rigidbody2D>().velocity = new Vector2(randomX, randomY).normalized * speedCorpses;
            c.transform.localScale = new Vector2(0.3f, 0.3f);
            m_CorpsesUsed.Add(c);
            
        }

    }

    public void AbsorberCuerpos()
    {
        foreach (GameObject c in m_CorpsesUsed)
        {
            c.GetComponent<ProyectilSack>().Return();
        }
    }

    public void DesactivarCuerpos()
    {
        foreach (GameObject c in m_CorpsesUsed)
        {
            c.SetActive(false);
        }

        m_CorpsesUsed.Clear();
    }

    public void ChangeFase(int i)
    {
        if (i == 1)
        {
            currentFaseAttacks = fase1Attacks;
            m_NavMeshAgent.speed = fase1Speed;
            m_BossHitboxDamage.Damage = fase1Damage;
        } else if (i == 2)
        {
            currentFaseAttacks = fase2Attacks;
            m_NavMeshAgent.speed = fase2Speed;
            m_BossHitboxDamage.Damage = fase2Damage;
        } else
        {
            currentFaseAttacks = fase3Attacks;
            m_NavMeshAgent.speed = fase3Speed;
            m_BossHitboxDamage.Damage = fase3Damage;
        }
    }
}
[Serializable]
public class AttacksCooldown
{
    public enum Attack { Attack1, Attack2, Attack3, Attack4}
    public Attack attack;
    public float cooldown;
    public bool needsRange;

    public AttacksCooldown()
    {
        attack = Attack.Attack1;
        cooldown = 0f;
        needsRange = true;
    }

    public AttacksCooldown(Attack a, float c, bool nr)
    {
        attack = a;
        cooldown = c;
        needsRange = nr;
    }

}
