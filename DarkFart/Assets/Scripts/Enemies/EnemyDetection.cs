using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour, IComparer<Collider2D>
{
    [SerializeField]
    private EnemyController controller;
    private EnemyRangeDetection rangeDetection;
    private float radius;
    [SerializeField] private bool isChasing;
    [SerializeField] private float secondsToWait = 0.5f;
    [SerializeField] private LayerMask layerMask;

    private void Start()
    {
        radius = controller.EnemySO.DetectionRange;
        GetComponent<CircleCollider2D>().radius = radius;
        rangeDetection = controller.gameObject.GetComponentInChildren<EnemyRangeDetection>();
    }
    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 8 || collision.gameObject.layer == 13)
        {
            controller.ChangeToChase(collision.transform);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 8 || collision.gameObject.layer == 13)
        {
            if(controller.gameObject.GetComponent<AttackFarm>() != null) 
            {
                controller.ChangeToCenter();
                return;
            }
            controller.ChangeToRandomWalk();
        }
    }*/

    //Trying a new method ---->

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isChasing) return;
        if (collision.gameObject.layer == 7 || collision.gameObject.layer == 8 || collision.gameObject.layer == 13)
        {
            StartCoroutine(WhoToChase());
        }
    }

    private IEnumerator WhoToChase()
    {
        isChasing = true;
        while (true)
        {
            yield return new WaitForSeconds(secondsToWait);

            if (rangeDetection.IsAttacking || controller.AttackingBarreras)
            {
                isChasing = false;
                StopAllCoroutines();
                break;
            }

            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius, layerMask);

            if(colliders.Length == 0)
            {
                isChasing = false;
                controller.ChangeToCenterOrRandom();
                StopAllCoroutines();
                break;
            }

            Array.Sort(colliders, this);
            controller.ChangeToChase(colliders[0].transform);
        }
    }

    public void StopAttacking()
    {
        StartCoroutine(WhoToChase());
    }

    public int Compare(Collider2D x, Collider2D y)
    {
        return (int)((x.transform.position - transform.position).magnitude - (y.transform.position - transform.position).magnitude);
    }

}
