using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(RandomWalkEnemy))]
public class EnemyController : MonoBehaviour
{
    private Animator m_Animator;
    private NavMeshAgent m_NavMeshAgent;

    [SerializeField]
    private Transform m_PlayerTransform;
    [SerializeField]
    private Transform m_PivotHitbox;
    [SerializeField] private Enemy m_EnemySO;
    public Enemy EnemySO
    {
        get { return m_EnemySO; }
        set { m_EnemySO = value; }
    }
    [SerializeField] private GameObject diana;
    

    private RandomWalkEnemy m_RandomWalkEnemy;
    private AttackFarm m_AttackFarm;
    private enum SwitchMachineStates { NONE, IDLE, CHASEPLAYER, ATTACK, RANDOMCENTER };
    private SwitchMachineStates CurrentState;

    [SerializeField] private LayerMask m_Barreras;
    private bool m_AttackingBarreras = false;
    public bool AttackingBarreras { get { return m_AttackingBarreras;} }

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == CurrentState)
            return;

        ExitState();
        InitState(newState);
    }
    private void InitState(SwitchMachineStates currentState)
    {
        CurrentState = currentState;
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                m_Animator.Play("enemy_idle");
                break;

            case SwitchMachineStates.CHASEPLAYER:

                m_Animator.Play("enemy_chase");
                StartCoroutine(Recalculate());

                break;

            case SwitchMachineStates.ATTACK:
                if (m_PlayerTransform == null)
                {
                    if (m_AttackingBarreras) m_AttackingBarreras = false;
                    ChangeToCenterOrRandom();
                    break;
                }
                m_PivotHitbox.up = m_PlayerTransform.position - transform.position;
                StartCoroutine(Attack());
                //m_Animator.Play("enemy_attack");
                break;

            case SwitchMachineStates.RANDOMCENTER:

                //m_Animator.Play("enemy_walk");
                break;
            default:
                break;
        }
    }
    private void ExitState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.CHASEPLAYER:
                if(m_NavMeshAgent.enabled)
                    m_NavMeshAgent.SetDestination(transform.position);
                StopAllCoroutines();
                break;

            case SwitchMachineStates.ATTACK:
                StopAllCoroutines();
                break;
            case SwitchMachineStates.RANDOMCENTER:

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:
                
                break;
            case SwitchMachineStates.CHASEPLAYER:

                break;
            case SwitchMachineStates.ATTACK:
                if (m_PlayerTransform == null)
                {
                    if(m_AttackingBarreras) m_AttackingBarreras = false;
                    ChangeToCenterOrRandom();
                    break;
                }
                m_PivotHitbox.up = m_PlayerTransform.position - transform.position;
                break;
            case SwitchMachineStates.RANDOMCENTER:

                break;
            default:
                break;
        }
    }

    

    void Awake()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_NavMeshAgent.updateUpAxis = false;
        m_NavMeshAgent.updateRotation = false;
        m_Animator = GetComponent<Animator>();
        m_RandomWalkEnemy = GetComponent<RandomWalkEnemy>();
        if(TryGetComponent<AttackFarm>(out AttackFarm a))
            m_AttackFarm = a;

        //Set Variables from SO
        SetSOVariables();
    }

    public void SetSOVariables()
    {
        m_NavMeshAgent.speed = m_EnemySO.Speed;
        GetComponent<VidaController>().Hp = m_EnemySO.Hp;
        GetComponent<VidaController>().MaxHP = m_EnemySO.Hp;
        GetComponent<EntitieDie>().Drops = m_EnemySO.Drops;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
        if (m_AttackFarm != null)
        {
            m_AttackFarm.GoToCenter();
            return;
        }
        m_RandomWalkEnemy.StartWalkRandomly();
    }

    void Update()
    {
        UpdateState();
    }

    private IEnumerator Recalculate()
    {
        while (true)
        {
            if(m_NavMeshAgent.enabled && m_PlayerTransform != null)
                m_NavMeshAgent.SetDestination(m_PlayerTransform.position);

            if (m_AttackingBarreras)
                yield return new WaitForSeconds(0.5f);

            if (m_NavMeshAgent.pathStatus == NavMeshPathStatus.PathPartial)
            {
                Vector3 dir = m_PlayerTransform.position - transform.position;
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, dir, dir.magnitude, m_Barreras, -1, 1);
                if (hit.transform == null) yield return new WaitForSeconds(0.5f);

                Debug.Log("Atacar Barrera");
                m_AttackingBarreras = true;
                //Hay que cambiar a idle primero porque sino hace return ya que newState == currentState
                //Creo que no hace falta cambiar a idle ahora porque antes la se�orita gisela habia pasado el transform por referencia en la corrutina
                //entonces por mucho que modificases la variable le daba igual pero ahora deberia ir pero no tengo tiempo pa testear y no quiero romperlo :)
                ChangeState(SwitchMachineStates.IDLE);
                ChangeToChase(hit.transform);
                break;
                //ChangeToAttack(hit.transform);
            }
            yield return new WaitForSeconds(0.5f);
        }

    }

    private IEnumerator Attack()
    {
        while(true)
        {
            m_Animator.Play("enemy_attack");
            yield return new WaitForSeconds(m_EnemySO.AttackCooldown);
        }
    }


    public void ChangeToChase(Transform t)
    {
        m_PlayerTransform = t;
        ChangeState(SwitchMachineStates.CHASEPLAYER);
        m_RandomWalkEnemy.StopWalkRandomly();
    }

    public void ChangeToCenterOrRandom()
    {
        ChangeState(SwitchMachineStates.RANDOMCENTER);
        if (m_AttackFarm != null)
        {
            m_AttackFarm.GoToCenter();
            return;
        }

        m_RandomWalkEnemy.StartWalkRandomly();
    }

    public void ChangeToAttack(Transform t)
    {
        m_PlayerTransform = t;
        ChangeState(SwitchMachineStates.ATTACK);
    }

    public void ActivateDiana()
    {
        diana.SetActive(true);
    }

    public void DesactivateDiana()
    {
        diana.SetActive(false);
    }

    public void WaitCooldown()
    {
        m_Animator.Play("enemy_cooldown");
    }

}
