using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EnterBoss : MonoBehaviour
{
    [SerializeField] private Tilemap m_bossTilemap;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 7)
        {
            m_bossTilemap.gameObject.SetActive(true);
        }
    }
}
