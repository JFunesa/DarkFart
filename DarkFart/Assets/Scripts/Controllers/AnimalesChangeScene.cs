using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimalesChangeScene : MonoBehaviour
{
    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        InteractPlayer player = FindFirstObjectByType<InteractPlayer>();
        if (player.lastAnimal != null)
        {
            Debug.Log("Animal contigo mamahuevo");
            GameObject g = Instantiate(player.lastAnimal);
            g.GetComponent<AnimalController>().Inicia(new Vector3(0, 0));
        }
    }
}
