using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XR;

public class CookController : MonoBehaviour
{
    public static CookController Instance { get; internal set; }
    [Header("GUI")]
    [SerializeField]
    private DisplayCookBook m_DisplayCookBook;
    [SerializeField]
    private DisplayCookRecipe m_DisplayCookRecipe;
    [SerializeField]
    private DropInCaldero m_DropInCaldero;
    [SerializeField]
    private WaterDisplay m_WaterDisplay;
    [Header("Cook")]
    [SerializeField]
    private List<CalderoBehaviour> m_CalderoBehaviour = new List<CalderoBehaviour>();
    [SerializeField]
    private int m_actualCaldero;
    [SerializeField]
    private CookBook m_recetas;
    [SerializeField]
    private InputController inputController;
    private TagDrop drop;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        inputController = FindAnyObjectByType<InputController>();
    }

    public int addCaldero(CalderoBehaviour caldero)
    {
        m_CalderoBehaviour.Add(caldero);
        return (m_CalderoBehaviour.Count - 1);
    }

    public void loadDisplayCookRecipe(int pos)
    {
        //Debug.Log(pos);
        m_DisplayCookRecipe.Load(m_recetas.GetCookRecipeSlot(pos));
    }

    public void cookBookVisible(int pos)
    {
        m_actualCaldero = pos;
        if(m_DisplayCookBook == null)
            m_DisplayCookBook = FindAnyObjectByType<DisplayCookBook>();
        m_DisplayCookBook.gameObject.SetActive(!m_DisplayCookBook.gameObject.activeInHierarchy);
        if (m_DisplayCookBook.gameObject.activeSelf == true)
        {
            m_DropInCaldero.setCaldero(m_CalderoBehaviour[m_actualCaldero]);
            m_WaterDisplay.setCaldero(m_CalderoBehaviour[m_actualCaldero]);
            m_WaterDisplay.mostrar();
            inputController.DisableDefault();
            guiController.Instance.disableAll();
            drop = FindAnyObjectByType<TagDrop>();
            drop.gameObject.SetActive(false);
        }
        else
        {
            inputController.EnableDefault();
            guiController.Instance.enableAll();
            if (drop != null)
                drop.gameObject.SetActive(true);
        }
    }

    public void cookBookVisible()
    {
        m_actualCaldero = 0;
        m_DisplayCookBook.gameObject.SetActive(!m_DisplayCookBook.gameObject.activeInHierarchy);
        if (m_DisplayCookBook.gameObject.activeSelf == true)
        {
            m_DropInCaldero.setCaldero(m_CalderoBehaviour[m_actualCaldero]);
            inputController.DisableDefault();
            if(guiController.Instance != null)
                guiController.Instance.disableAll();
        }
        else
        {
            inputController.EnableDefault();
            if (guiController.Instance != null)
                guiController.Instance.enableAll();
        }
    }

    public DisplayCookBook getDisplayCraft()
    {
        return m_DisplayCookBook;
    }

    public void setDisplayCookBook(DisplayCookBook displayCraftBook)
    {
        m_DisplayCookBook = displayCraftBook;
    }

    public CookBook getCookBook()
    {
        return m_recetas;
    }

    public WaterDisplay getWaterDisplay()
    {
        return m_WaterDisplay;
    }

    public bool Cook(Item item, int water)
    {
        if (m_CalderoBehaviour[m_actualCaldero].tryCook(water))
        {
            InventarioController.Instance.AddItem(item);
            return true;
        }

        return false;

    }
}
