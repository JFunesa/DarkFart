using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    private Rigidbody2D m_Rigidbody;
    private NavMeshAgent m_NavMeshAgent;
    private float displacementDist = 2f;
    [SerializeField] private float wait;

    [Header("NPC Values")]
    [SerializeField] private new string name;
    public string Name {  get { return name; } set { name = value; } }
    [SerializeField] private float hp;
    [SerializeField] private float speed;
    public float Speed { get { return speed; } set { speed = value; } }
    [SerializeField] private PersonalidadInfo personality;
    public PersonalidadInfo Personality { get { return personality; } set {  personality = value; } }
    [SerializeField] private float friendship;
    public float Friendship { get { return friendship; } set { friendship = value; } }
    [SerializeField] private List<Item> drops;
    [SerializeField] private bool isInGranja = false;
    public bool IsInGranja { get { return isInGranja; } set { isInGranja = value; } }

    private Transform m_PlayerTransform;
    private Transform m_EnemyTransform;

    public enum SwitchMachineStates { NONE, IDLE, RANDOMWALK };
    public SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
        Debug.Log("State: " + newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                Debug.Log("NPCState: IDLE");

                break;

            case SwitchMachineStates.RANDOMWALK:
                Debug.Log("NPCState: RANDOMWALK");
                StartCoroutine(WalkRandomly());
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.RANDOMWALK:
                StopAllCoroutines();
                break;



            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;
            case SwitchMachineStates.RANDOMWALK:

                break;


            default:
                break;
        }
    }


    void Awake()
    {

        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_NavMeshAgent.updateUpAxis = false;
        m_NavMeshAgent.updateRotation = false;
    }

    private void Start()
    {
        m_NavMeshAgent.speed = speed + (speed * (friendship / 100));
        GetComponent<VidaController>().Hp = hp + (hp * (friendship / 100));
        GetComponent<VidaController>().MaxHP = hp + (hp * (friendship / 100));
        GetComponent<InteractNPC>().Personalidad = personality;

        GetComponent<EntitieDie>().Drops = drops;

        InitState(SwitchMachineStates.RANDOMWALK);
    }

    void Update()
    {
        UpdateState();
    }

    private IEnumerator WalkRandomly()
    {
        while (true)
        {
            int rx = Random.Range(-1, 2);
            int ry = Random.Range(-1, 2);

            Vector3 dir = new Vector3(rx, ry, 0).normalized;
            
            m_NavMeshAgent.SetDestination(transform.position - (dir * displacementDist));

            yield return new WaitForSeconds(wait);
        }
    }

    public void ChangeToRandomWalk()
    {
        Debug.Log("a randomwalkear");
        m_EnemyTransform = null;
        ChangeState(SwitchMachineStates.RANDOMWALK);
    }

    public void StopWalkingRandomly()
    {
        ExitState();
        m_CurrentState = SwitchMachineStates.NONE;
        
    }

    public void StopWalking()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void AddFriendship(float f)
    {
        friendship += f;
        m_NavMeshAgent.speed = speed + (speed * (friendship / 100));
        GetComponent<VidaController>().Hp = hp + (hp * (friendship / 100));
        GetComponent<VidaController>().MaxHP = hp + (hp * (friendship / 100));
    }

    public void TpToGranja()
    {
        transform.position = Vector3.zero;
        gameObject.GetComponent<IdInfo>().SaveInfo();
        Destroy(gameObject);
    }
}
