using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterDisplay : MonoBehaviour
{
    private Image barraVida;

    [SerializeField]
    private CalderoBehaviour water;

    void Awake()
    {
        barraVida = GetComponent<Image>();
    }

    public void setCaldero(CalderoBehaviour caldero)
    {
        water = caldero;
    }

    public void mostrar()
    {
        if (barraVida != null)
            barraVida.fillAmount = ((float) water.mirarWater()) / ((float) water.mirarWaterMax());
    }

}
