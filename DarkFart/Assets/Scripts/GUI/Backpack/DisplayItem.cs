using TMPro;
using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class DisplayItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Functionality")]
    [SerializeField]
    private GameEventItem m_Event;

    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_AmountText;
    [SerializeField]
    private Image m_Image;
    [SerializeField]
    private Image m_Selected;
    [SerializeField]
    private GameObject m_positionDescripcion;

    [SerializeField]
    private Sprite m_SpriteDefault;

    [SerializeField]
    private int posBackpack;

    [SerializeField]
    private GameObject m_Packing;

    [SerializeField]
    private Item m_Item;

    [Header("Controller")]
    [SerializeField]
    private InventarioController m_InventarioController;

    public void setController(InventarioController controller)
    {
        m_InventarioController = controller;
    }
    public GameObject getPacking()
    {
        return m_Packing;
    }

    public Item GetItem()
    {
        return m_Item;
    }

    public void setItem(Item item)
    {
        m_Item = item;
    }

    private void Start()
    {
        m_InventarioController = InventarioController.Instance;
    }

    public string getText() { return m_Text.text; }
    public string getAmount() { return m_AmountText.text; }
    public Sprite getImage() { return m_Image.sprite; }
    public GameObject getGameObject() { return gameObject; }

    public Image getSelected() { return m_Selected; }

    public void Load(Item item)
    {
        m_Item = item;
        m_Text.text = item.Name;
        if (item.Sprite != null)
            m_Image.sprite = item.Sprite;
        else
            m_Image.sprite = m_SpriteDefault;

        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(() => RaiseEvent(item));
    }

    public void Load(Backpack.ItemSlot itemSlot)
    {
        m_Packing.SetActive(true);
        if (itemSlot.Item != null && itemSlot.Item.Name != null)
        {
            Load(itemSlot.Item);
            //m_Text.gameObject.SetActive(true);
            m_AmountText.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_AmountText.text = itemSlot.Amount.ToString();
        }
        else
        {
            m_Item = null;
            //m_Text.gameObject.SetActive(false);
            m_AmountText.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
        }
    }

    public void setPos(int pos)
    {
        posBackpack = pos;
    }

    public int getPosBackpack()
    {
        return posBackpack;
    }

    private void RaiseEvent(Item item)
    {
        //m_Event?.Raise(item);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (m_Item)
        {
            switch (m_Item)
            {
                case Arma arma:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"ATK: {arma.Dmg}\nCD: {arma.Cooldown}", $"CR%: {arma.ProbCritico}\nEC: {arma.EnergiaQueGasta}");
                    break;
                case Armadura armadura:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"EHP: {armadura.HpExtra}");
                    break;
                case Cania cania:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"WT: {cania.WaitingTime}");
                    break;
                case Galleda galleda:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"WATER:", $"{galleda.Water}/{galleda.maxWater}");
                    break;
                case Regadera regadora:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"WATER:", $"{regadora.Water}/{regadora.maxWater}");
                    break;
                case HachaPico hachaPico:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"ATK: {hachaPico.Dmg}\nEC: {hachaPico.EnergiaQueGasta}");
                    break;
                case Equipable equipable:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"EC: {equipable.EnergiaQueGasta}");
                    break;
                case Alimento alimento:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name, $"CAL: {alimento.Calories}");
                    break;
                default:
                    m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().loadItem(m_Item.Name);
                    break;
            }
            m_InventarioController.getDescripcionItem().transform.position = m_positionDescripcion.transform.position;
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (m_Item)
        {
            m_InventarioController.getDescripcionItem().GetComponent<DescripcionItem>().desactivate();
        }

    }
}

