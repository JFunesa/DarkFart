using UnityEngine;

public class DisplayBackpack : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayBackpackParent;

    [SerializeField]
    private GameObject m_DisplayArmaduraParent;

    [SerializeField]
    private GameObject m_DisplayItemPrefab;

    private void Start()
    {
        RefreshAllBackpack();
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayBackpackParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        for (int i = 0; i < InventarioController.Instance.getBackpack().ItemSlots2.Count; i++)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplayBackpackParent.transform);
            displayedItem.GetComponent<DisplayItem>().Load(InventarioController.Instance.getBackpack().ItemSlots2[i]);
            displayedItem.GetComponent<DisplayItem>().setPos(i);
            displayedItem.GetComponent<DisplayItem>().setController(InventarioController.Instance);
            displayedItem.GetComponent<DragAndDrop>().setController(InventarioController.Instance);
            //m_InventarioController.getAll();
        }
        if (m_DisplayArmaduraParent != null)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplayArmaduraParent.transform);
            displayedItem.GetComponent<DisplayItem>().Load(InventarioController.Instance.getBackpack().GetArmadura());
            displayedItem.GetComponent<DisplayItem>().setPos(InventarioController.Instance.getBackpack().GetPosSlotArmadura());
            displayedItem.GetComponent<DisplayItem>().setController(InventarioController.Instance);
            displayedItem.GetComponent<DragAndDrop>().setController(InventarioController.Instance);
            InventarioController.Instance.LoadArmadura(InventarioController.Instance.getBackpack().GetArmadura().Item);
        }
        
    }

    private void LoadEverything()
    {
        DisplayItem[] displayeds = m_DisplayBackpackParent.GetComponentsInChildren<DisplayItem>();
        for (int i = 0; i < displayeds.Length; i++)
        {
            displayeds[i].GetComponent<DisplayItem>().Load(InventarioController.Instance.getBackpack().ItemSlots2[i]);
        }
        if (m_DisplayArmaduraParent != null && InventarioController.Instance.getBackpack().GetArmadura() != null && m_DisplayArmaduraParent.GetComponentInChildren<DisplayItem>() != null)
        {
            m_DisplayArmaduraParent.GetComponentInChildren<DisplayItem>().Load(InventarioController.Instance.getBackpack().GetArmadura());
            InventarioController.Instance.LoadArmadura(InventarioController.Instance.getBackpack().GetArmadura().Item);
        }
    }

    public void RefreshBackpack()
    {
        LoadEverything();
    }

    public void RefreshAllBackpack()
    {
        ClearDisplay();
        FillDisplay();
    }
}
