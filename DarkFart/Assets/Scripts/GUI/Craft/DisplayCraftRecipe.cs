using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCraftRecipe : MonoBehaviour
{
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private Image m_Image;
    [SerializeField]
    private TextMeshProUGUI m_Description;
    [SerializeField]
    private Sprite m_SpriteDefault;
    [SerializeField]
    private GameObject m_Button;

    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayIngredientParent;
    [SerializeField]
    private GameObject m_DisplayIngredinetPrefab;

    [Header("Controller")]
    [SerializeField]
    private CraftController m_CraftController;

    public void Load(CraftBook.CraftRecipe craft)
    {
        if (craft != null && craft.recipe.Result.Name != null && craft.unlocked)
        {
            m_Text.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_Description.gameObject.SetActive(true);
            m_Button.gameObject.SetActive(true);
            m_Text.text = craft.recipe.Result.Name;
            m_Description.text = craft.recipe.Description;
            if (craft.recipe.Result.Sprite != null)
                m_Image.sprite = craft.recipe.Result.Sprite;
            else
                m_Image.sprite = m_SpriteDefault;

            m_Button.GetComponent<Button>().onClick.RemoveAllListeners();
            m_Button.GetComponent<Button>().onClick.AddListener(() => Craft(craft));
            RefreshIngredients(craft);
        }
        else
        {
            ClearDisplay();
            m_Text.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
            m_Description.gameObject.SetActive(false);
            m_Button.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        m_CraftController = CraftController.Instance;
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayIngredientParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay(CraftBook.CraftRecipe craft)
    {
        for (int i = 0; i < craft.recipe.IngredientsCraft.Count; i++)
        {
            GameObject displayedItem = Instantiate(m_DisplayIngredinetPrefab, m_DisplayIngredientParent.transform);
            displayedItem.GetComponent<DisplayIngridient>().Load(craft.recipe.IngredientsCraft[i], QuantityCalcule(craft.recipe.IngredientsCraft[i]));
        }
    }

    public void RefreshIngredients(CraftBook.CraftRecipe craft)
    {
        ClearDisplay();
        FillDisplay(craft);
    }

    public int QuantityCalcule(Craft.Ingredients ingredient)
    {
        int quantityItem = 0;
        for (int i = 0; i < InventarioController.Instance.getBackpack().ItemSlots2.Count; i++)
        {
            if (InventarioController.Instance.getBackpack().ItemSlots2[i].Item == ingredient.ingredient)
            {
                quantityItem += InventarioController.Instance.getBackpack().ItemSlots2[i].Amount;
            }
        }

        return quantityItem;
    }

    public bool QuantitySubstraction(Craft.Ingredients ingredient)
    {
        List<int> listInts = new List<int>();
        for (int i = 0; i < InventarioController.Instance.getBackpack().ItemSlots2.Count; i++)
        {
            if (InventarioController.Instance.getBackpack().ItemSlots2[i].Item == ingredient.ingredient)
            {
                listInts.Add(i);
            }
        }
        int quantity = ingredient.quantity;
        int quantityInBackpack = 0;
        int listQuantity = listInts.Count;
        int j = 0;
        Debug.Log(listInts.Count);
        while (quantity > 0 && j < listQuantity)
        {
            quantityInBackpack = InventarioController.Instance.getBackpack().ItemSlots2[listInts[j]].Amount;
            Debug.Log("Quantity Substraction: " + ingredient.ingredient + " - " + quantity + " - " + quantityInBackpack);
            InventarioController.Instance.getBackpack().RemoveAmountByPos(listInts[j], quantity);
            quantity -= quantityInBackpack;

            j++;
        }
        if (quantity == 0)
            return true;
        else
            return false;
    }

    private bool CanCraft(Craft.Ingredients ingredient)
    {
        int quantityItem = QuantityCalcule(ingredient);
        if (quantityItem >= ingredient.quantity)
        {
            return true;
        }
        return false;
    }

    public void Craft(CraftBook.CraftRecipe craft)
    {
        bool cancraft = true;
        for (int i = 0; i < craft.recipe.IngredientsCraft.Count; i++)
        {
            if (cancraft)
                cancraft = CanCraft(craft.recipe.IngredientsCraft[i]);
        }
        if (cancraft)
        {
            for (int i = 0; i < craft.recipe.IngredientsCraft.Count; i++)
            {
                QuantitySubstraction(craft.recipe.IngredientsCraft[i]);
            }
            RefreshIngredients(craft);
            m_CraftController.Craft(craft.recipe.Result);
        }
        else
            Debug.Log("Faltan materiales");
    }



}
