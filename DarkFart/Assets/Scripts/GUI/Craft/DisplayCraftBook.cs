using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisplayCraftBook : MonoBehaviour
{
    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayRecipesParent;
    [SerializeField]
    private GameObject m_DisplayRecipePrefab;

    [Header("Controller")]
    [SerializeField]
    private CraftController m_CraftController;
    [Header("Pages")]
    [SerializeField]
    private int[] m_pageQuantity = new int[4];
    [SerializeField]
    private int m_pageIndex = 0;

    private void OnEnable()
    {
        if (m_CraftController == null)
            m_CraftController = CraftController.Instance;
        LoadP1();
    }

    private void Start()
    {
        m_CraftController = CraftController.Instance;
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayRecipesParent.transform)
            Destroy(child.gameObject);
    }

    public void LoadP1()
    {
        ClearDisplay();
        int ini = 0;
        int end = m_CraftController.getCraftBook().recipesSlot.Count;
        for (int i = ini; i < end; i++)
        {
            if (m_CraftController.getCraftBook().recipesSlot[i] != null)
            {
                GameObject displayedItem = Instantiate(m_DisplayRecipePrefab, m_DisplayRecipesParent.transform);
                displayedItem.GetComponent<DisplayCraftItem>().Load(m_CraftController.getCraftBook().recipesSlot[i]);
                displayedItem.GetComponent<DisplayCraftItem>().setPos(i);
                displayedItem.GetComponent<DisplayCraftItem>().setController(m_CraftController);
            }
        }
    }

    public void LoadP2()
    {
        ClearDisplay();
        int ini = m_pageQuantity[0];
        int end = (m_pageQuantity[0] + m_pageQuantity[1]) - 1;
        for (int i = ini; i < end; i++)
        {
            if (m_CraftController.getCraftBook().recipesSlot[i] != null)
            {
                GameObject displayedItem = Instantiate(m_DisplayRecipePrefab, m_DisplayRecipesParent.transform);
                displayedItem.GetComponent<DisplayCraftItem>().Load(m_CraftController.getCraftBook().recipesSlot[i]);
                displayedItem.GetComponent<DisplayCraftItem>().setPos(i);
                displayedItem.GetComponent<DisplayCraftItem>().setController(m_CraftController);
            }
        }
    }
    public void LoadP3()
    {
        ClearDisplay();
        int ini = m_pageQuantity[0] + m_pageQuantity[1];
        int end = (m_pageQuantity[0] + m_pageQuantity[1] + m_pageQuantity[2]) - 1;
        for (int i = ini; i < end; i++)
        {
            if (m_CraftController.getCraftBook().recipesSlot[i] != null)
            {
                GameObject displayedItem = Instantiate(m_DisplayRecipePrefab, m_DisplayRecipesParent.transform);
                displayedItem.GetComponent<DisplayCraftItem>().Load(m_CraftController.getCraftBook().recipesSlot[i]);
                displayedItem.GetComponent<DisplayCraftItem>().setPos(i);
                displayedItem.GetComponent<DisplayCraftItem>().setController(m_CraftController);
            }
        }
    }

    public void LoadP4()
    {
        ClearDisplay();
        int ini = m_pageQuantity[0] + m_pageQuantity[1] + m_pageQuantity[2];
        int end = (m_pageQuantity[0] + m_pageQuantity[1] + m_pageQuantity[2] + m_pageQuantity[3]) - 1;
        for (int i = ini; i < end; i++)
        {
            if (m_CraftController.getCraftBook().recipesSlot[i] != null)
            {
                GameObject displayedItem = Instantiate(m_DisplayRecipePrefab, m_DisplayRecipesParent.transform);
                displayedItem.GetComponent<DisplayCraftItem>().Load(m_CraftController.getCraftBook().recipesSlot[i]);
                displayedItem.GetComponent<DisplayCraftItem>().setPos(i);
                displayedItem.GetComponent<DisplayCraftItem>().setController(m_CraftController);
            }
        }
    }

    public void LoadP5()
    {
        ClearDisplay();
        int ini = m_pageQuantity[0] + m_pageQuantity[1] + m_pageQuantity[2] + m_pageQuantity[3];
        int end = (m_pageQuantity[0] + m_pageQuantity[1] + m_pageQuantity[2] + m_pageQuantity[3] + m_pageQuantity[4]) - 1;
        for (int i = ini; i < end; i++)
        {
            if(m_CraftController.getCraftBook().recipesSlot[i] != null)
            {
                GameObject displayedItem = Instantiate(m_DisplayRecipePrefab, m_DisplayRecipesParent.transform);
                displayedItem.GetComponent<DisplayCraftItem>().Load(m_CraftController.getCraftBook().recipesSlot[i]);
                displayedItem.GetComponent<DisplayCraftItem>().setPos(i);
                displayedItem.GetComponent<DisplayCraftItem>().setController(m_CraftController);
            }
        }
    }

    public void craftBookVisible()
    {
        m_CraftController.craftBookVisible();
    }
}
