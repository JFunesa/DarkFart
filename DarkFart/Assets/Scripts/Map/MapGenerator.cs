using UnityEngine;
using System;
using UnityEngine.Tilemaps;
using System.Collections;
using System.Collections.Generic;
using NavMeshPlus.Components;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_NavMesh;
    [SerializeField] private bool m_Verbose = false;
    [SerializeField] private MapInfo m_MapInfo;
    [SerializeField] private int chunkWidth = 30;

    [Header("Prefabs")]
    [SerializeField] private GameObject m_Animal;
    [SerializeField] private GameObject m_Enemy;
    [SerializeField] private GameObject m_Item;
    [SerializeField] private GameObject m_ArbolRoca;
    [SerializeField]private GameObject m_ItemWorld;

    [Header("Lists")]
    [SerializeField] private List<CityGenerator.TascaGameObject> m_NPCTasks;
    [SerializeField] private List<TypeAnimalGameobject> m_TypeAnimalGameObjects;
    [SerializeField] private List<CitySpawnInfo> m_CitySpawnInfos;

    
    [SerializeField]
    private LayerMask m_ItemSpawnMask;
    [SerializeField]
    private GameObject farm;

    [Serializable]
    public struct Bioma
    {
        public BiomaInfo bioma;
        public float distance;
    }
    public Bioma[] biomes;

    [SerializeField]
    private Tilemap m_Tilemap;

    [Header("Size")]
    //size of the area we will paint
    [SerializeField]
    private int m_Width;
    [SerializeField]
    private int m_Height;

    [Header("Base Parameters")]
    [SerializeField]
    //offset from the perlin map
    private float m_OffsetX;
    [SerializeField]
    private float m_OffsetY;
    [SerializeField]
    private float m_Frequency = 4f;
    [SerializeField]
    private float m_FrequencyResource = 4f;
    [SerializeField]
    private float m_FrequencyWater = 4f;
    [SerializeField]
    private float m_FrequencyEnemy = 4f;

    //octaves
    private const int MAX_OCTAVES = 8;
    [Header("Octave Parameters")]
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_Octaves = 0;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_OctavesResource = 0;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_OctavesWater = 0;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_OctavesEnemy = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_Lacunarity = 2;
    [Range(2, 3)]
    [SerializeField]
    private int m_LacunarityResource = 2;
    [Range(2, 3)]
    [SerializeField]
    private int m_LacunarityWater = 2;
    [Range(2, 3)]
    [SerializeField]
    private int m_LacunarityEnemy = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_Persistence = 0.5f;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_PersistenceResource = 0.5f;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_PersistenceWater = 0.5f;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_PersistenceEnemy = 0.5f;
    [SerializeField]
    [Min(1)]
    private int m_TilemapToPerlinUnits;
    [SerializeField]
    private int m_Soroll;

    private float[] dangerRadios;
    public static MapGenerator Instance { get; internal set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        ChangeWidth();
        GenerateChunks();
        ChunkController.Instance.ChunksCreated();

        TilemapController.PlantasTilemap.ClearAllTiles();
        m_Tilemap = TilemapController.PlantasTilemap;
        dangerRadios = new float[biomes.Length];
        for (int i = 0; i < biomes.Length; i++)
        {
            dangerRadios[i] = (m_Height / 2) * biomes[i].distance;
        }

        if (m_MapInfo.AlreadyGenerated)
        {
            RecreateGameObjects();
        }

        GenerateTerrain();
        ChunkController.Instance.ChunksFilled();
        m_MapInfo.AlreadyGenerated = true; 
        StartCoroutine(SpawnAllTowns());
        m_NavMesh.GetComponent<NavMeshSurface>().BuildNavMesh();
    }

    private void RecreateGameObjects()
    {
        foreach (ItemInfo info in m_MapInfo.items)
        {
            GameObject g = Instantiate(m_Item);

            g.GetComponent<ItemInWorld>().SetItem(info.item, info.amount);
            g.GetComponent<IdInfo>().Id = info.id;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
            ChunkController.Instance.InsertIntoChunk(g);
        }
        foreach (AnimalInfo info in m_MapInfo.animals)
        {
            GameObject g = Instantiate(m_Animal);

            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<AnimalController>().animal = info.animal;
            g.GetComponent<EntitieDie>().Drops = info.items;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
            ChunkController.Instance.InsertIntoChunk(g);
        }

        foreach (EnemyInfo info in m_MapInfo.enemies)
        {
            GameObject g = Instantiate(m_Enemy);

            g.GetComponent<EnemyController>().EnemySO = info.enemy;
            g.GetComponent<EntitieDie>().Drops = info.items;
            g.GetComponent<IdInfo>().Id = info.id;

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
            ChunkController.Instance.InsertIntoChunk(g);
        }
        foreach (DropeableInfo info in m_MapInfo.resources)
        {
            GameObject g = Instantiate(m_ArbolRoca);
            g.GetComponent<IdInfo>().Id = info.id;
            g.GetComponent<EntitieDie>().Drops = new List<Item>(info.items);

            g.transform.position = info.pos;
            g.GetComponent<SpriteRenderer>().sprite = info.sprite;
            g.layer = info.layer;
            ChunkController.Instance.InsertIntoChunk(g);
        }
    }

    private void ChangeWidth()
    {
        if (m_Width % chunkWidth != 0)
        {
            int aux = m_Width % chunkWidth;
            aux = chunkWidth - aux;
            m_Width += aux;
        }
        if (m_Height % chunkWidth != 0)
        {
            int aux = m_Height % chunkWidth;
            aux = chunkWidth - aux;
            m_Height += aux;
        }
        Chunk.width = chunkWidth;
    }

    private void GenerateChunks()
    {
        ChunkController.Instance.Clear();

        int id = 0;
        int f = 0;
        int c = 0;
        for (int i = -m_Width * m_TilemapToPerlinUnits / 2; i < m_Width * m_TilemapToPerlinUnits / 2; i += chunkWidth)
        {
            c = 0;
            for (int j = -m_Height * m_TilemapToPerlinUnits / 2; j < m_Height * m_TilemapToPerlinUnits / 2; j += chunkWidth)
            {
                ChunkController.Instance.Add(new Chunk(id, new Vector3(i, j, 0), f, c));
                id++;
                c++;
            }
            f++;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            GenerateTerrain();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            SpawnTown(farm, Vector3.zero);
        }
    }

    private void GenerateTerrain()
    {
        m_Tilemap.ClearAllTiles();

        dangerRadios = new float[biomes.Length];
        for (int i = 0; i < biomes.Length; i++)
        {
            dangerRadios[i] = (m_Height / 2) * biomes[i].distance;
        }

        for (int y = -m_Height / 2; y < m_Height / 2; y++)
        {
            for (int x = -m_Width / 2; x < m_Width / 2; x++)
            {
                Bioma currentBioma = biomes[biomes.Length - 1];
                float distancewFromFarm = new Vector2(x, y).magnitude + UnityEngine.Random.Range(-m_Soroll, m_Soroll);
                for (int i = 0; i < biomes.Length; i++)
                {
                    if (distancewFromFarm < dangerRadios[i])
                    {
                        currentBioma = biomes[i];
                        break;
                    }
                }
                float perlin = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency, m_Width, m_Height, m_OffsetX, m_OffsetY, m_Octaves, m_Lacunarity, m_Persistence);

                Tile tile = currentBioma.bioma.tiles[0].tile;
                BiomaInfo.ResourceSpawn[] resourceSpawns = currentBioma.bioma.tiles[0].resources;
                BiomaInfo.ItemSpawn[] itemSpawns = currentBioma.bioma.tiles[0].items;
                BiomaInfo.NPCSpawn[] npcSpawn = currentBioma.bioma.tiles[0].npcs;
                BiomaInfo.AnimalSpawn[] animalSpawn = currentBioma.bioma.tiles[0].animals;
                float spawnProbability = 0;
                float spawnItemProbability = 0;
                float spawnNPCProbability = 0;
                float spawnAnimalProbability = 0;
                for (int i = 0; i < currentBioma.bioma.tiles.Length; i++)
                {
                    if (perlin < currentBioma.bioma.tiles[i].probability)
                    {
                        spawnProbability = currentBioma.bioma.tiles[i].resourceProbability;
                        spawnItemProbability = currentBioma.bioma.tiles[i].itemProbability;
                        spawnNPCProbability = currentBioma.bioma.tiles[i].NPCProbability;
                        spawnAnimalProbability = currentBioma.bioma.tiles[i].animalProbability;
                        resourceSpawns = currentBioma.bioma.tiles[i].resources;
                        itemSpawns = currentBioma.bioma.tiles[i].items;
                        npcSpawn = currentBioma.bioma.tiles[i].npcs;
                        animalSpawn = currentBioma.bioma.tiles[i].animals;
                        tile = currentBioma.bioma.tiles[i].tile;
                        break;
                    }
                }
                float perlinWater = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_FrequencyWater, m_Width, m_Height, m_OffsetX - 100, m_OffsetY - 100, m_OctavesWater, m_LacunarityWater, m_PersistenceWater);
                if (perlinWater < currentBioma.bioma.water.waterProbability)
                {
                    tile = currentBioma.bioma.water.waterTile;
                }
                

                for (int i = 0; i < m_TilemapToPerlinUnits; i++)
                    for (int j = 0; j < m_TilemapToPerlinUnits; j++)
                    {
                        Vector3Int pos = new Vector3Int(m_TilemapToPerlinUnits * x + i, m_TilemapToPerlinUnits * y + j, 0);
                        m_Tilemap.SetTile(pos, tile);
                        if (m_MapInfo.AlreadyGenerated) continue;
                        if (tile != currentBioma.bioma.water.waterTile)
                        {
                            Vector3 posIRL = new Vector3(m_TilemapToPerlinUnits * x + i, m_TilemapToPerlinUnits * y + j, 0);
                            bool resSpawned = false;

                            //Resources
                            perlin = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_FrequencyResource, m_Width, m_Height, m_OffsetX + 100, m_OffsetY + 100, m_OctavesResource, m_LacunarityResource, m_PersistenceResource);
                            if (perlin <= spawnProbability)
                            {

                                for (int k = 0; k < resourceSpawns.Length; k++)
                                {
                                    if (UnityEngine.Random.Range(0, 101) < resourceSpawns[k].spawnProbability)
                                    {
                                        GameObject g = Instantiate(resourceSpawns[k].resource);
                                        g.transform.position = m_Tilemap.GetCellCenterWorld(m_Tilemap.WorldToCell(posIRL));
                                        resSpawned = true;
                                        ChunkController.Instance.InsertIntoChunk(g);
                                        break;
                                    }
                                }
                            }

                            //Item
                            if (!resSpawned) SpawnItems(posIRL, spawnItemProbability, itemSpawns);

                            //NPC
                            if (!resSpawned) SpawnNPCs(posIRL, spawnNPCProbability, npcSpawn, currentBioma.bioma);

                            //Animal
                            if (!resSpawned) SpawnAnimal(posIRL, spawnAnimalProbability, animalSpawn);

                            //Enemies
                            if (!resSpawned) SpawnEnemies(currentBioma, posIRL, x, y);

                        }
                    }

            }
        }
    }

    //TODO: Hacer la funcion de respawn de items
    public void RespawnItems()
    {
        for (int y = -m_Height / 2; y < m_Height / 2; y++)
        {
            for (int x = -m_Width / 2; x < m_Width / 2; x++)
            {
                Bioma currentBioma = biomes[biomes.Length - 1];
                float distancewFromFarm = new Vector2(x, y).magnitude + UnityEngine.Random.Range(-m_Soroll, m_Soroll);
                for (int i = 0; i < biomes.Length; i++)
                {
                    if (distancewFromFarm < dangerRadios[i])
                    {
                        currentBioma = biomes[i];
                        break;
                    }
                }
                float spawnItemProbability = 0;
                for (int i = 0; i < currentBioma.bioma.tiles.Length; i++)
                {
                    
                }


            }
        }
    }
    public void SpawnItems(Vector3 target, float spawnItemProbability, BiomaInfo.ItemSpawn[] itemSpawns)
    {

        Vector3 origin = target;
        origin.z = -10;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_ItemSpawnMask);

        if (hit.transform != null) return;

        if (UnityEngine.Random.Range(0, 101) < spawnItemProbability)
        {
            for (int k = 0; k < itemSpawns.Length; k++)
            {
                if (UnityEngine.Random.Range(0, 101) < itemSpawns[k].spawnProbability)
                {
                    Item i = itemSpawns[k].item;
                    GameObject g = Instantiate(m_ItemWorld);
                    g.GetComponent<ItemInWorld>().Inicia(i, target);
                    ChunkController.Instance.InsertIntoChunk(g);
                    break;
                }
            }
        }
    }

    public void SpawnNPCs(Vector3 target, float spawnNPCProbability, BiomaInfo.NPCSpawn[] nPCSpawns, BiomaInfo currentBioma)
    {
        Vector3 origin = target;
        origin.z = -10;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_ItemSpawnMask);

        if (hit.transform != null) return;

        if (UnityEngine.Random.Range(0, 101) < spawnNPCProbability)
        {
            for (int k = 0; k < nPCSpawns.Length; k++)
            {
                if (UnityEngine.Random.Range(0, 101) < nPCSpawns[k].spawnProbability)
                {
                    PersonalidadInfo personalidad = nPCSpawns[k].personalidad;
                    TascaInfo.Tasca t = nPCSpawns[k].tasques[0].tasca;
                    int erp = UnityEngine.Random.Range(1, 101);
                    for (int j = 0; j < nPCSpawns[k].tasques.Length; ++j)
                    {
                        if (erp < nPCSpawns[k].tasques[j].probability)
                        {
                            t = nPCSpawns[k].tasques[j].tasca;
                            break;
                        }
                        
                    }
                    GameObject g = Instantiate(CityGenerator.FindGameObjectByTasca(t, m_NPCTasks));
                    g.GetComponent<NPCController>().Personality = personalidad;
                    g.GetComponent<NPCController>().Name = currentBioma.NpcNames[UnityEngine.Random.Range(0, currentBioma.NpcNames.Length)];
                    g.transform.position = target;
                    ChunkController.Instance.InsertIntoChunk(g);
                    break;
                }
            }
        }
    }

    public void SpawnAnimal(Vector3 target, float spawnNPCProbability, BiomaInfo.AnimalSpawn[] animalSpawns)
    {
        Vector3 origin = target;
        origin.z = -10;

        RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.forward, 20, m_ItemSpawnMask);

        if (hit.transform != null) return;

        if (UnityEngine.Random.Range(0, 101) < spawnNPCProbability)
        {
            for (int k = 0; k < animalSpawns.Length; k++)
            {
                if (UnityEngine.Random.Range(0, 101) < animalSpawns[k].spawnProbability)
                {
                    GameObject g = Instantiate(FindGameObjectByTypeAnimal(animalSpawns[k].typeAnimal, m_TypeAnimalGameObjects));
                    g.transform.position = target;
                    ChunkController.Instance.InsertIntoChunk(g);
                    break;
                }
            }
        }
    }


    public void SpawnEnemies(Bioma currentBioma, Vector3 posIRL, int x, int y)
    {
        float perlinEnemy = UnityEngine.Random.Range(0, 101);
        if (perlinEnemy < currentBioma.bioma.enemyProbability)
        {
            for (int k = 0; k < currentBioma.bioma.enemies.Length; k++)
            {
                if (UnityEngine.Random.Range(0, 101) < currentBioma.bioma.enemies[k].enemyProbability)
                {
                    for (int l = 0; l < currentBioma.bioma.enemies[k].enemyAmount; ++l)
                    {
                        GameObject g = Instantiate(m_Enemy);
                        g.GetComponent<EnemyController>().EnemySO = currentBioma.bioma.enemies[k].enemy;
                        g.transform.position = posIRL;
                        ChunkController.Instance.InsertIntoChunk(g);
                    }
                    break;
                }
            }
        }
    }
    public void SpawnTown(GameObject town, Vector3 pos)
    {
        Vector3 origin = pos;
        origin.z = -10;

        Vector3 size = new Vector3(6, 6, 1);

        RaycastHit2D[] hits = Physics2D.BoxCastAll(origin, size/2, 0f, Vector3.forward, 20, m_ItemSpawnMask);

        foreach (RaycastHit2D hit in hits)
        {
            Destroy(hit.transform.gameObject);
        }

        GameObject g = Instantiate(town);
        g.transform.position = pos;
        ChunkController.Instance.InsertIntoChunk(g);
    }

    private IEnumerator SpawnAllTowns()
    {
        yield return new WaitForSeconds(0.1f);
        SpawnTown(farm, Vector3.zero);
        foreach (CitySpawnInfo c in m_CitySpawnInfos)
        {
            Vector2 randomVector = GenerateRandomVectorOnCircle(c.distanceSpawn);
            SpawnTown(c.prefab, randomVector);
        }
    }
    private Vector2 GenerateRandomVectorOnCircle(float radius)
    {
        float randomAngle = UnityEngine.Random.Range(0f, Mathf.PI * 2f);

        float x = Mathf.Cos(randomAngle) * radius;
        float y = Mathf.Sin(randomAngle) * radius;

        return new Vector2(x, y);
    }

    public GameObject FindGameObjectByTypeAnimal(BiomaInfo.TypeAnimal typeAnimal, List<TypeAnimalGameobject> list)
    {
        for (int i = 0; i < list.Count; ++i)
        {
            if (typeAnimal == list[i].typeAnimal) return list[i].prefab;
        }
        return null;
    }
}

[Serializable]
public class TypeAnimalGameobject
{
    public BiomaInfo.TypeAnimal typeAnimal;
    public GameObject prefab;
    public TypeAnimalGameobject()
    {
        typeAnimal = BiomaInfo.TypeAnimal.NONE;
        prefab = null;
    }

    public TypeAnimalGameobject(BiomaInfo.TypeAnimal t, GameObject p)
    {
        typeAnimal = t;
        prefab = p;
    }

}
[Serializable]
public class CitySpawnInfo
{
    public float distanceSpawn;
    public GameObject prefab;
    public CitySpawnInfo()
    {
        distanceSpawn = 0;
        prefab = null;
    }

    public CitySpawnInfo(float t, GameObject p)
    {
        distanceSpawn = t;
        prefab = p;
    }

}
