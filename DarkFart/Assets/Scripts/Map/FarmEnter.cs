using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FarmEnter : MonoBehaviour
{
    [SerializeField] private MapInfo MapInfo;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MapInfo.Save();
        SceneController.Instance.ChangeScene(SceneController.SCENES.Granja);
    }
}
