using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(DamageInfo))]
[RequireComponent(typeof(FollowTransform))]
public class BulletController : MonoBehaviour
{
    [SerializeField] private LayerMask collisions;
    private Rigidbody2D rigidbody;
    private DamageInfo damageInfo;
    private CircleCollider2D collider;
    private FollowTransform followTransform;
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        damageInfo = GetComponent<DamageInfo>();
        collider = GetComponent<CircleCollider2D>();
        followTransform = GetComponent<FollowTransform>();
        followTransform.enabled = false;
    }
    public void Init(Vector3 pos, Vector2 direction, float dmg, float radius, float spd, float dis, bool isAble = false, float capacity = 0)
    {
        Init(pos, direction, spd, dmg, radius, dis);
        if (isAble)
        {
            Follow(Physics2D.OverlapCircle(pos, dis, collisions)?.transform, capacity, spd);
        }
    }

    public void Init(Vector3 pos, Vector2 direction, float dmg, float radius, float spd, float dis, Transform follow, bool isAble = false, float capacity = 0)
    {
        Init(pos, direction, spd, dmg, radius, dis);
        if (isAble)
        {
            Follow(follow, capacity, spd);
        }
    }

    private void Follow(Transform t, float d, float s)
    {
        followTransform.follow = t;
        followTransform.degrees = d;
        followTransform.speed = s;
        if (t == null) return;
        followTransform.enabled = true;
    }

    private void Init(Vector3 pos, Vector2 direction, float spd, float dmg, float radius, float dis)
    {
        damageInfo.damage = dmg;
        collider.radius = radius;
        transform.position = pos;
        gameObject.SetActive(true);
        rigidbody.velocity = direction.normalized * spd;
        //As the bullet may change the direction and check the position is inefficient and imprecise we will make with the time the bullet will take to do the distance
        StartCoroutine(Disable(dis / spd));
    }

    private IEnumerator Disable(float wait)
    {
        yield return new WaitForSeconds(wait);
        followTransform.enabled = false;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        transform.up = rigidbody.velocity;
    }
}
