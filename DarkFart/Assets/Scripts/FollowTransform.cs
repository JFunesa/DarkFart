using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    public Transform follow;
    public float degrees;
    public float speed;
    private Rigidbody2D rigidbody;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if(follow == null)
        {
            this.enabled = false;
            return;
        }
        Vector3 target = follow.position - transform.position;
        rigidbody.velocity = Vector3.RotateTowards(rigidbody.velocity, target, degrees * Mathf.Deg2Rad * Time.deltaTime, 1f).normalized * speed;
    }
}
