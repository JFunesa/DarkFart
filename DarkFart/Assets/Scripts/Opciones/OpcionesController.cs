using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XInput;

public class OpcionesController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_buttons;
    public void visibility()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(!transform.GetChild(i).gameObject.activeInHierarchy);
        }
        for (int i = 0; i < m_buttons.transform.childCount; i++)
        {
            m_buttons.transform.GetChild(i).gameObject.SetActive(!m_buttons.transform.GetChild(i).gameObject.activeInHierarchy);
        }
    }
}
