using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    private List<GameObject> pooledObject = new List<GameObject>();
    [SerializeField] private int amountPool = 8;
    [SerializeField] private Transform transform = null;

    [SerializeField] private GameObject obje;
    void Start()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj;
            if (transform != null)
                obj = Instantiate(obje, transform);
            else
                obj = Instantiate(obje);
            obj.SetActive(false);
            pooledObject.Add(obj);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObject.Count; i++)
        {
            if (!pooledObject[i].activeInHierarchy)
            {
                return pooledObject[i];
            }
        }
        return null;
    }
    public GameObject CleanPool()
    {
        for (int i = 0; i < pooledObject.Count; i++)
        {
            if (pooledObject[i].activeInHierarchy)
            {
                pooledObject[i].SetActive(false);
            }
        }
        return null;
    }

}
