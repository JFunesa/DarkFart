using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.Windows;

[RequireComponent(typeof(InputController))]
public class InventoryControllerTest : MonoBehaviour
{
    private InputController _controller;
    private Vector2 delta;
    [SerializeField]private int m_CurrentInventory = 1;
    private int m_LastInventory;
    public int CurrentInventory { get { return m_CurrentInventory; } }
    [SerializeField]private float m_Sensivity = 120f;
    private void Awake()
    {
        _controller = GetComponent<InputController>();
    }

    private void Update()
    {
        delta = _controller.Delta.ReadValue<Vector2>();
        if(delta.y>0)
        {
            while(delta.y > 0)
            {
                delta.y -= m_Sensivity;
                m_CurrentInventory--;
                if (m_CurrentInventory <= 0)
                    m_CurrentInventory = 9;
            }
        }else if(delta.y<0)
        {
            while (delta.y < 0)
            {
                delta.y += m_Sensivity;
                m_CurrentInventory++;
                if (m_CurrentInventory >= 10)
                    m_CurrentInventory = 1;
            }
        }
    }

    public void QuickChange(InputAction.CallbackContext context)
    {
        m_CurrentInventory = m_LastInventory;
    }

    public void ChangeToOne(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 1) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 1;
        //TODO que se vea el item seleccionado
    }
    public void ChangeToTwo(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 2) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 2;
    }
    public void ChangeToThree(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 3) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 3;
    }
    public void ChangeToFour(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 4) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 4;
    }
    public void ChangeToFive(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 5) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 5;
    }
    public void ChangeToSix(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 6) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 6;
    }
    public void ChangeToSeven(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 7) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 7;
    }
    public void ChangeToEight(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 8) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 8;
    }
    public void ChangeToNine(InputAction.CallbackContext context)
    {
        if (m_CurrentInventory == 9) return;
        m_LastInventory = m_CurrentInventory;
        m_CurrentInventory = 9;
    }
}
