using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDie : MonoBehaviour, IDieable
{
    public void Die()
    {
        PlantsGrowths[] plants = GameObject.FindObjectsOfType<PlantsGrowths>();

        foreach (PlantsGrowths plant in plants)
        {
            Destroy(plant.gameObject);
        }
        TimeManager.PlayerDied();
    }
}
