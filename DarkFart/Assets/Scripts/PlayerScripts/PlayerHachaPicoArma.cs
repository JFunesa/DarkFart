using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

[RequireComponent(typeof(MovimentPlayer))]
public class PlayerHachaPicoArma : MonoBehaviour, IComparer<Collider2D>
{
    [SerializeField] private HachaPicoController hacha;
    [SerializeField] private HachaPicoController pico;
    [SerializeField] private HachaPicoController arma;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float m_RangVisio;
    private EnemyController enemy;
    private MovimentPlayer player;
    private ObjectPool pool;
    private int actualEnemy = 1;

    private void Awake()
    {
        player = GetComponent<MovimentPlayer>();
        pool = GetComponent<ObjectPool>();
    }
    public void DoitPico(float da�o)
    {
        if(enemy == null) 
            pico.Doit(player.LastInput, da�o);
        else
            pico.Doit((enemy.transform.position - transform.position).normalized , da�o);
    }
    public void DoitHacha(float da�o)
    {
        if(enemy == null)
            hacha.Doit(player.LastInput, da�o);
        else
            hacha.Doit((enemy.transform.position - transform.position).normalized , da�o);
    }

    public void DoitArma(float da�o, float cooldown, float area, Arma a)
    {
        arma.ChangeCollider(area);
        StartCoroutine(Cooldown(cooldown, a));
        if(enemy == null) 
            arma.Doit(player.LastInput, da�o);
        else
            arma.Doit((enemy.transform.position - transform.position).normalized , da�o);
    }

    public void DoitArmaDistancia(float[] values, bool isAble, Arma a)
    {
        StartCoroutine(Cooldown(values[1], a));
        if(enemy == null)
            pool.GetPooledObject().GetComponent<BulletController>().Init(transform.position, player.LastInput, values[0], values[2], values[3], values[4], isAble, values[5]);
        else
            pool.GetPooledObject().GetComponent<BulletController>().Init(transform.position, (enemy.transform.position - transform.position).normalized, values[0], values[2], values[3], values[4], enemy.transform, isAble, values[5]);
    }

    private IEnumerator Cooldown(float wait, Arma a)
    {
        yield return new WaitForSeconds(wait);
        a.isAble = true;
    }

    public void FixEnemy(InputAction.CallbackContext context)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, m_RangVisio, layerMask);
        Array.Sort(colliders, this);
        if (enemy == null)
        {
            FirstEnemy(colliders);
        }
        else
        {
            if(actualEnemy < colliders.Length)
            {
                NextEnemy(colliders);
            }
            else
            {
                FirstEnemy(colliders);
            }
        }
        Debug.Log(enemy.gameObject.name);
    }

    private void NextEnemy(Collider2D[] colliders)
    {
        for (int i = actualEnemy; i < colliders.Length; i++)
        {
            if (colliders[i].TryGetComponent<EnemyController>(out EnemyController controller))
            {
                if (controller != enemy)
                {
                    ChangeEnemy(controller, i + 1);
                    return;
                }

            }
        }
        FirstEnemy(colliders);
    }

    private void FirstEnemy(Collider2D[] colliders)
    {
        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent<EnemyController>(out EnemyController controller))
            {
                ChangeEnemy(controller, 1);
                return;
            }
        }
    }
    private void ChangeEnemy(EnemyController e, int a)
    {
        if (enemy != null)
            enemy.DesactivateDiana();
        enemy = e;
        enemy.ActivateDiana();
        actualEnemy = a;
    }
    public void QuitEnemy(InputAction.CallbackContext context)
    {
        if(enemy != null)
            enemy.DesactivateDiana();
        enemy = null;
        actualEnemy = 1;
    }

    public int Compare(Collider2D x, Collider2D y)
    {
        return (int) ((x.transform.position - transform.position).magnitude - (y.transform.position - transform.position).magnitude);
    }
}
