using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(VidaController))]
[RequireComponent(typeof(GameEventFloatListener))]
public class HambreController : MonoBehaviour
{
    [SerializeField] private float hambre;
    public float Hambre { get { return hambre; } set { hambre = value; } }
    [SerializeField] private float Maxhambre;
    public float MaxHambre { get { return Maxhambre; } set { Maxhambre = value; } }
    [SerializeField] private float time;
    [SerializeField] private bool starving = false;
    [SerializeField] private HambreBarra GUI;

    private IEnumerator WasteHambre()
    {
        while (hambre > 0)
        {
            yield return new WaitForSeconds(time);
            hambre--;
            RefreshGUI();
        }
        hambre = 0;
        starving = true;
        gameObject.GetComponent<VidaController>().Starving();
        StopAllCoroutines();
    }

    public float getHambre()
    {
        return hambre;
    }

    public float getMaxHambre()
    {
        return Maxhambre;
    }

    public void EatFood(float food)
    {
        hambre += food;
        if (starving)
        {
            gameObject.GetComponent<VidaController>().StopStarving();
            StartCoroutine(WasteHambre());
            starving = false;
        }
        if (hambre > Maxhambre) { hambre = Maxhambre; }
        RefreshGUI();
    }
    private void RefreshGUI()
    {
        if(GUI != null)
            GUI.mostrar();
    }
    public void startAll()
    {
        GUI = FindFirstObjectByType<HambreBarra>();
        StartCoroutine(WasteHambre());
    }
}
