using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicLoader : MonoBehaviour
{
    [SerializeField]
    public float distanceThreshold = 10;      //low number for testing purpose, you could set this to your camera's far plane.
    [SerializeField]
    private GameObject[] enemy;
    [SerializeField]
    private GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        enemy = GameObject.FindGameObjectsWithTag("Enemy");
    }
    void Update()
    {
        foreach (GameObject planet in enemy)
        {
            if ((player.transform.position.magnitude - planet.transform.position.magnitude) > distanceThreshold)
                planet.SetActive(false);
            else
                planet.SetActive(true);
        }
    }
}
