using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFrontBackSide : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rigidbody;
    [SerializeField] private Sprite[] sprites;
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector3 vel = rigidbody.velocity;

        if (Mathf.Abs(vel.x) > Mathf.Abs(vel.y)) 
        {
            spriteRenderer.sprite = sprites[2];
            if(vel.x < 0)
                spriteRenderer.flipX = true;
            else
                spriteRenderer.flipX = false;
        }else if(vel.y < 0) 
        {
            spriteRenderer.sprite = sprites[0];
        }
        else if (vel.y > 0)
        {
            spriteRenderer.sprite= sprites[1];
        }
    }
}
