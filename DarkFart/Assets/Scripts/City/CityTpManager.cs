using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityTpManager : MonoBehaviour
{
    public static CityTpManager Instance { get; internal set; }
    public Dictionary<int, Vector3> tpList = new Dictionary<int, Vector3>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void AddEntradaToTpList(int id, Vector3 t)
    {
        tpList.Add(id, t);
    }

    public Vector3 GetTransformFromId(int tpId)
    {
        return tpList[tpId];
    }
}
