using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaBehaviour : MonoBehaviour, Interactuable, EstructuraBehaviour
{
    [SerializeField] private GameEvent m_IWantToSleep;
    private Estructura estructura;
    public Estructura Estructura { get { return estructura; } set { estructura = value; } }

    public void Interact()
    {
        m_IWantToSleep.Raise();
    }
}
