using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EstructuraBehaviour 
{
    public Estructura Estructura { get; set; }
}
