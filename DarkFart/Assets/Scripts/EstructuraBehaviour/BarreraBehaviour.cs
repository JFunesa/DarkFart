using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarreraBehaviour : MonoBehaviour, EstructuraBehaviour
{
    private Estructura estructura;
    public Estructura Estructura { get => estructura; set => estructura = value; }

    private void Awake()
    {
        BarrerasController.Instance.Barreras.Add(GetComponent<VidaController>());
    }

    private void OnDestroy()
    {
        BarrerasController.Instance.Barreras.Remove(GetComponent<VidaController>());
    }
}