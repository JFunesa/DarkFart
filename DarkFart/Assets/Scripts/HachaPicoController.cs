using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HachaPicoController : MonoBehaviour
{
    private DamageInfo damageInfo;
    private BoxCollider2D collider;

    private void Awake()
    {
        damageInfo = GetComponentInChildren<DamageInfo>(true);
        collider = GetComponentInChildren<BoxCollider2D>(true);
        gameObject.SetActive(false);
    }

    [SerializeField] private float duration = 0.25f;
    public void Doit(Vector2 dir, float da�o)
    {
        damageInfo.damage = da�o; 
        float cos = Mathf.Round(Mathf.Acos(dir.x) * 180 / Mathf.PI);
        float sen = Mathf.Round(Mathf.Asin(dir.y) * 180 / Mathf.PI);
        if(sen < 0) sen = 360 + sen;
        if(cos != sen)
        {
            float newCos = 360 - cos;
            if(newCos == sen) 
            {
                cos = newCos;
            }
            else
            {
                float newSen;
                if (dir.y >= 0)
                {
                    newSen = 180 - sen;
                }
                else 
                {
                    newSen = 360 + 360 - sen - 180;
                }
                if (newSen == newCos)
                    cos = newCos;
                else if (newSen != cos && newSen != newCos)
                {
                    float uno = Math.Abs(cos - sen);
                    float dos = Math.Abs(cos - newSen);
                    float tres = Math.Abs(newCos - sen);
                    float cuatro = Math.Abs(newCos - newSen);

                    if(!((uno < tres && uno < cuatro) || (dos < tres && dos < cuatro))) 
                        cos = newCos;
                }
            }
        }
        Vector3 rotation = Vector3.zero;
        rotation.z = cos;
        transform.eulerAngles = rotation;
        gameObject.SetActive(true);
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive (false);
    }

    public void ChangeCollider(float area)
    {
        collider.size = new Vector2(area, area);
    }
}
